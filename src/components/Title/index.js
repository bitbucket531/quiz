import React, { useContext } from "react";
import { SiteTheme } from "../../context";

const Title = ({ title, question, questionNumber }) => {
  const { theme } = useContext(SiteTheme);

  return (
    <React.Fragment>
      <div className="title-wrapper">
        <div className="col-3"></div>
        <div className={`title__text col-6 title__stripe--${theme}`}>
          <p> {title}</p>
        </div>
        <div
          className={
            question
              ? "title__question title__question--show"
              : "title__question"
          }
        >
          <div
            className={`title__question-number title__question-number--${theme}`}
          >
            <p>TWÓJ WYNIK</p>
            <p>{questionNumber}/10</p>
          </div>
        </div>
        <div className="col-2"></div>
      </div>
    </React.Fragment>
  );
};

export default Title;
