import React from "react";
import { Link } from "react-router-dom";

import logo from "../../assets/Q.png";
import title from "../../assets/quiz_logo.png";
import back from "../../assets/button_back.png";
import close from "../../assets/button_close.png";

const Navigation = ({ backwardPath }) => {
  return (
    <React.Fragment>
      <div className="navigation-wrapper">
        <div className="navigation__logo col-2">
          <img src={logo} alt="Nope" />
          <div className="navigation__buttons--button">
            <Link to={backwardPath}>
              <img src={back} alt="Nope" />
            </Link>
          </div>
        </div>
        <div className="navigation__title col-8">
          <img src={title} alt="Nope" />
        </div>
        <div className="navigation__buttons col-2">
          <div className="navigation__buttons--button">
            <Link to={backwardPath}>
              <img src={back} alt="Nope" />
            </Link>
          </div>
          <div className="navigation__buttons--button">
            <Link to="/">
              <img src={close} alt="Nope" onClick={() => window.close()} />
            </Link>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Navigation;
