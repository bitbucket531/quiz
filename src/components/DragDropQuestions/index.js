import React, { useContext, useEffect, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Redirect } from "react-router-dom";
import { SiteTheme } from "../../context";

const DragDropQuestions = ({ forwardPath }) => {
  const {
    theme,
    questionNumber,
    changeQuestion,
    jsonQuestions,
    score,
    setScore,
  } = useContext(SiteTheme);

  var booleanka = false;

  useEffect(() => {
    if (booleanka === true) setScore(score + 1);
  }, [booleanka]);

  const list1 = [];
  const list2 = [1, 2, 3, 4, 5];

  const [redirect, setRedirect] = useState(false);

  var questions = jsonQuestions[theme];
  var keys2 = Object.keys(questions);
  var answers = questions[keys2[questionNumber - 1]];

  const renderList1 = () => {
    return list1.map((element, index) => {
      const stringIndex = String(element);
      return (
        <Draggable draggableId={stringIndex} index={element}>
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              className={`select__question start-button-wrapper start__button--${theme}`}
            >
              <p>{answers[element - 1]}</p>
            </div>
          )}
        </Draggable>
      );
    });
  };

  const renderList2 = () => {
    return list2.map((element, index) => {
      const stringIndex = String(element);
      return (
        <Draggable draggableId={stringIndex} index={element}>
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              className={`select__question start-button-wrapper start__button--${theme}`}
            >
              <p>{answers[element - 1]}</p>
            </div>
          )}
        </Draggable>
      );
    });
  };

  const changeOrder = (draggableId) => {
    list1.push(list2[list2.indexOf(Number(draggableId))]);
    console.log(list1);
    list2.splice(list2.indexOf(Number(draggableId)), 1);
  };

  function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  const checkCorrectAnswer = (count) => {
    if (answers[5] === count) {
      document
        .getElementsByClassName("question__correct-answer")[0]
        .classList.add("correct-answer--show");
      booleanka = true;
    } else {
      document
        .getElementsByClassName("question-button")[0]
        .classList.add("select__question--wrong");
    }
    changeOrder(count);
    sleep(1200).then(() => {
      changeQuestion(questionNumber + 1);
      setRedirect(true);
    });
  };

  const renderRedirect = () => {
    if (redirect) {
      return <Redirect to={forwardPath} />;
    }
  };

  const onDragEnd = (result) => {
    const { destination, draggableId } = result;

    if (!destination) return;

    if (destination.droppableId !== "list1") return;
    if (list1.length === 0) checkCorrectAnswer(Number(draggableId));
  };

  return (
    <React.Fragment>
      {renderRedirect()}
      <DragDropContext onDragEnd={onDragEnd}>
        <div className="select__question--question">
          <p>{keys2[questionNumber - 1]}</p>
        </div>

        <Droppable droppableId="list1">
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.droppableProps}
              className={`question-button start-button-wrapper question__button--${theme}`}
            >
              {renderList1()}
              {provided.placeholder}

              <div
                className={`question__correct-answer question__correct-answer--${theme}`}
              ></div>
            </div>
          )}
        </Droppable>

        <Droppable droppableId="list2">
          {(provided) => (
            <div
              ref={provided.innerRef}
              {...provided.droppableProps}
              className="select-question-wrapper col-8"
            >
              {renderList2()}

              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </React.Fragment>
  );
};

export default DragDropQuestions;
