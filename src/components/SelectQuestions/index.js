import React, { useContext, useState } from "react";
import { Redirect } from "react-router-dom";
import { SiteTheme } from "../../context";

const SelectQuestions = ({ forwardPath }) => {
  const {
    theme,
    questionNumber,
    changeQuestion,
    jsonQuestions,
    score,
    setScore,
  } = useContext(SiteTheme);
  const [redirect, setRedirect] = useState(false);
  const [questionAvaible, setQuestionAvaible] = useState(true);
  var questions = jsonQuestions[theme];
  var keys2 = Object.keys(questions);
  var answers = questions[keys2[questionNumber - 1]];

  const correctAnswersRefs = [];
  for (let index = 0; index < 5; index++) {
    correctAnswersRefs[index] = React.createRef();
  }

  const questionRefs = [];
  for (let index = 0; index < 5; index++) {
    questionRefs[index] = React.createRef();
  }

  // function sleep(milliseconds) {
  //   const date = Date.now();
  //   let currentDate = null;
  //   do {
  //     currentDate = Date.now();
  //   } while (currentDate - date < milliseconds);
  // }

  function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  const checkCorrectAnswer = (count) => {
    console.log(correctAnswersRefs[0].current, questionRefs);
    if (questionAvaible) {
      setQuestionAvaible(false);
      if (answers[5] == count) {
        setScore(score + 1);
        document
          .getElementsByClassName("question__correct-answer" + count)[0]
          .classList.add("correct-answer--show");
      } else {
        document
          .getElementsByClassName("question-" + count)[0]
          .classList.add("select__question--wrong");
      }
      sleep(1200).then(() => {
        changeQuestion(questionNumber + 1);
        setRedirect(true);
      });
    }
  };

  const renderRedirect = () => {
    if (redirect) {
      return <Redirect to={forwardPath} />;
    }
  };

  return (
    <React.Fragment>
      {renderRedirect()}
      <div className="select__question--question">
        <p>{keys2[questionNumber - 1]}</p>
      </div>
      <div className="select-question-wrapper col-8">
        <div
          className={`select__question question-1 start-button-wrapper start__button--${theme}`}
          onClick={() => checkCorrectAnswer(1)}
        >
          <p>{answers[0]}</p>
          <div
            ref={correctAnswersRefs[0]}
            className={`question__correct-answer1 question__correct-answer--${theme}`}
          ></div>
        </div>
        <div
          className={`select__question question-2 start-button-wrapper start__button--${theme}`}
          onClick={() => checkCorrectAnswer(2)}
        >
          <p>{answers[1]}</p>
          <div
            ref={correctAnswersRefs[1]}
            className={`question__correct-answer2 question__correct-answer--${theme}`}
          ></div>
        </div>
        <div
          className={`select__question question-3 start-button-wrapper start__button--${theme}`}
          onClick={() => checkCorrectAnswer(3)}
        >
          <p>{answers[2]}</p>
          <div
            ref={correctAnswersRefs[2]}
            className={`question__correct-answer3 question__correct-answer--${theme}`}
          ></div>
        </div>
        <div
          className={`select__question question-4 start-button-wrapper start__button--${theme}`}
          onClick={() => checkCorrectAnswer(4)}
        >
          <p>{answers[3]}</p>
          <div
            ref={correctAnswersRefs[3]}
            className={`question__correct-answer4 question__correct-answer--${theme}`}
          ></div>
        </div>
        <div
          className={`select__question question-5 start-button-wrapper start__button--${theme}`}
          onClick={() => checkCorrectAnswer(5)}
        >
          <p>{answers[4]}</p>
          <div
            ref={correctAnswersRefs[4]}
            className={`question__correct-answer5 question__correct-answer--${theme}`}
          ></div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default SelectQuestions;
