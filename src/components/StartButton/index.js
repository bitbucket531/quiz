import React, { useContext } from "react";
import { SiteTheme } from "../../context";

import start_button_programowanie from "../../assets/programowanie/rozpocznij/arrow_play.png";
import start_button_kultura from "../../assets/kultura/rozpocznij/arrow_play.png";
import start_button_technologia from "../../assets/technologia/rozpocznij/arrow_play.png";
import start_button_motoryzacja from "../../assets/motoryzacja/rozpocznij/arrow_play.png";
import start_button_historia from "../../assets/historia/rozpocznij/arrow_play.png";

const StartButton = ({text}) => {
  const { theme } = useContext(SiteTheme);

  const displayArrowPlay = () => {
    switch (theme) {
      case "programming":
        return <img src={start_button_programowanie}></img>;
      case "culture":
        return <img src={start_button_kultura}></img>;
      case "motorization":
        return <img src={start_button_motoryzacja}></img>;
      case "technology":
        return <img src={start_button_technologia}></img>;
      case "history":
        return <img src={start_button_historia}></img>;
    }
  };

  return (
    <div className={`start-button-wrapper start__button--${theme}`}>
      <p>{text}</p>
      {displayArrowPlay()}
    </div>
  );
};

export default StartButton;
