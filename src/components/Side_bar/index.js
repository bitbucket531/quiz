import React, { useContext } from "react";
import { SiteTheme } from "../../context";
import { Link } from "react-router-dom";

import pink from "../../assets/programowanie/rozpocznij/linia_pink.png";
import ikona_programowanie from "../../assets/programowanie/rozpocznij/ikona_programowanie.png";
import ikona_kultura from "../../assets/kultura/rozpocznij/ikona_kultura.png";
import ikona_technologia from "../../assets/technologia/rozpocznij/ikona_technologia.png";
import ikona_motoryzacja from "../../assets/motoryzacja/rozpocznij/ikona_motoryzacja.png";
import ikona_historia from "../../assets/historia/rozpocznij/ikona_historia.png";

const Side_bar = () => {
  const { theme, setTheme } = useContext(SiteTheme);
  const forwardPath = "/start";

  const changeTheme = (newTheme) => {
    setTheme(newTheme);
  };

  const renderCategory = (category) => {
    if (category !== theme)
      switch (category) {
        case "programming":
          return (
            <Link to={forwardPath}>
              <div
                className={`chosen-category chosen-category--${theme}`}
                onClick={() => changeTheme("programming")}
              >
                <img
                  className="category__image1"
                  src={ikona_programowanie}
                ></img>
                <img className="category__image2" src={pink}></img>
                <p>PROGRAMOWANIE</p>
              </div>
            </Link>
          );
        case "culture":
          return (
            <Link to={forwardPath}>
              <div
                className={`chosen-category chosen-category--${theme}`}
                onClick={() => changeTheme("culture")}
              >
                <img className="category__image1" src={ikona_kultura}></img>
                <img className="category__image2" src={pink}></img>
                <p>KULTURA</p>
              </div>
            </Link>
          );
        case "history":
          return (
            <Link to={forwardPath}>
              <div
                className={`chosen-category chosen-category--${theme}`}
                onClick={() => changeTheme("history")}
              >
                <img className="category__image1" src={ikona_historia}></img>
                <img className="category__image2" src={pink}></img>
                <p>HISTORIA</p>
              </div>
            </Link>
          );
        case "motorization":
          return (
            <Link to={forwardPath}>
              <div
                className={`chosen-category chosen-category--${theme}`}
                n
                onClick={() => changeTheme("motorization")}
              >
                <img className="category__image1" src={ikona_motoryzacja}></img>
                <img className="category__image2" src={pink}></img>
                <p>MOTORYZACJA</p>
              </div>
            </Link>
          );
        case "technology":
          return (
            <Link to={forwardPath}>
              <div
                className={`chosen-category chosen-category--${theme}`}
                onClick={() => changeTheme("technology")}
              >
                <img className="category__image1" src={ikona_technologia}></img>
                <img className="category__image2" src={pink}></img>
                <p>TECHNOLOGIA</p>
              </div>
            </Link>
          );
        default:
          break;
      }
  };

  return (
    <div className="side-bar-wrapper">
      {renderCategory("programming")}
      {renderCategory("culture")}
      {renderCategory("motorization")}
      {renderCategory("technology")}
      {renderCategory("history")}
    </div>
  );
};

export default Side_bar;
