import React, { createContext, useState } from "react";

export const SiteTheme = createContext({
  theme: "programming",
  setTheme: (item) => {},
  previousTheme: "programming",
  setPreviousTheme: (item) => {},
  actualSite: 1,
  setActualSite: (item) => {},
  previousSite: 1,
  setPreviousSite: (item) => {},
  questionNumber: 1,
  changeQuestion: (item) => {},
  jsonQuestions: require("../questions.json"),
  score: 0,
  setScore: (item) => {},
});

export const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState("programming");
  const [previousTheme, setPreviousTheme] = useState("programming");
  const [actualSite, setActualSite] = useState(1);
  const [previousSite, setPreviousSite] = useState(1);
  const [questionNumber, changeQuestion] = useState(1);
  const [jsonQuestions] = useState(require("../questions.json"));
  const [score, setScore] = useState(0);

  return (
    <SiteTheme.Provider
      value={{
        theme,
        setTheme,
        previousTheme,
        setPreviousTheme,
        actualSite,
        setActualSite,
        previousSite,
        setPreviousSite,
        questionNumber,
        changeQuestion,
        jsonQuestions,
        score,
        setScore,
      }}
    >
      {children}
    </SiteTheme.Provider>
  );
};
