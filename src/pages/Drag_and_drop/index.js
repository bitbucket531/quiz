import React, { useContext } from "react";
import { SetBackground } from "../../services/SetBackground";
import { SiteTheme } from "../../context";

import Title from "../../components/Title";
import Navigation from "../../components/Navigation";
import DragDropQuestions from "../../components/DragDropQuestions";

const Drag_and_drop = () => {
  const title = "PRZESUŃ I UPUŚĆ POPRAWNĄ ODPOWIEDŹ";
  const backwardPath = "/start";
  let forwardPath = "/select";
  const { questionNumber } = useContext(SiteTheme);

  SetBackground(4);

  if (questionNumber === 10) {
    forwardPath = "/score";
  }

  return (
    <React.Fragment>
      <div className="drag-and-drop-wrapper">
        <Navigation backwardPath={backwardPath} forwardPath={forwardPath} />
        <Title title={title} question={true} questionNumber={questionNumber} />
        <DragDropQuestions forwardPath={forwardPath} />
      </div>
    </React.Fragment>
  );
};

export default Drag_and_drop;
