import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { SetBackground } from "../../services/SetBackground";
import { SiteTheme } from "../../context";

import Title from "../../components/Title";
import Navigation from "../../components/Navigation";
import ChoosenCategory from "../../services/ChosenCategory";
import Side_bar from "../../components/Side_bar";
import StartButton from "../../components/StartButton";

const Score = () => {
  const title = "TWÓJ WYNIK";
  const backwardPath = "/start";
  const forwardPath = "/score";
  const exactPath = "/score";
  const { theme, score, setScore, changeQuestion } = useContext(SiteTheme);

  SetBackground(5);

  const resetStats = () => {
    setScore(0);
    changeQuestion(1);
  };

  return (
    <React.Fragment>
      <div className="score-wrapper">
        <Navigation
          backwardPath={backwardPath}
          forwardPath={forwardPath}
          exactPath={exactPath}
        />
        <div className="score-inner-wrapper">
          <div className={`title__text col-9 title__stripe--${theme}`}>
            <p>{title}</p>
          </div>
          <div className="col-3"></div>
          <div className="col-6 result-wrapper">
            <div className="category-wrapper">{<ChoosenCategory />}</div>

            <div className="title__question title__question--show">
              <div
                className={`title__question-number title__question-number--${theme}`}
              >
                <p>TWÓJ WYNIK</p>
                <p>{score}/10</p>
              </div>
            </div>
            <Link
              className="start__button"
              to="/start"
              onClick={() => resetStats()}
            >
              <StartButton text="POWTÓRZ QUIZ" />
            </Link>
          </div>
          <div className="col-3">
            <p>WYBIERZ INNĄ KATEGORIĘ</p>
            <Side_bar />
          </div>
        </div>
        {/*
        <div className="result-wrapper">
          <Title title={title} />
          <p>{score}/10</p>
        </div>
        <div className="categories-wrapper">

        </div> */}
      </div>
    </React.Fragment>
  );
};

export default Score;
