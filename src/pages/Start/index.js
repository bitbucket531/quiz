import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { SetBackground } from "../../services/SetBackground";
import { SiteTheme } from "../../context";

import Title from "../../components/Title";
import Navigation from "../../components/Navigation";
import ChoosenCategory from "../../services/ChosenCategory";
import StartButton from "../../components/StartButton";

const Start = () => {
  const title = "WYBRANA KATEGORIA";
  const backwardPath = "/";
  const forwardPath = "/select";
  const exactPath = "/start";
  const { theme, changeQuestion, setScore } = useContext(SiteTheme);

  changeQuestion(1);
  setScore(0);
  SetBackground(2);

  return (
    <React.Fragment>
      <div className="start-wrapper">
        <Navigation
          backwardPath={backwardPath}
          forwardPath={forwardPath}
          exactPath={exactPath}
        />
        <Title title={title} question={false} />
        <div className="category-wrapper">{<ChoosenCategory />}</div>
        <Link className="start__button" to="/select">
          <StartButton text="ROZPOCZNIJ" />
        </Link>
      </div>
    </React.Fragment>
  );
};

export default Start;
