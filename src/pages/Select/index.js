import React, { useContext } from "react";
import { SetBackground } from "../../services/SetBackground";
import { SiteTheme } from "../../context";

import Title from "../../components/Title";
import Navigation from "../../components/Navigation";
import SelectQuestions from "../../components/SelectQuestions";

const Select = () => {
  const title = "WYBIERZ POPRAWNĄ ODPOWIEDŹ";
  const backwardPath = "/start";
  let forwardPath = "/drag";
  const { questionNumber } = useContext(SiteTheme);

  SetBackground(3);

  if (questionNumber === 10) {
    forwardPath = "/score";
  }

  return (
    <React.Fragment>
      <div className="select-wrapper">
        <Navigation backwardPath={backwardPath} forwardPath={forwardPath} />
        <Title title={title} question={true} questionNumber={questionNumber} />
        <SelectQuestions forwardPath={forwardPath} />
      </div>
    </React.Fragment>
  );
};

export default Select;
