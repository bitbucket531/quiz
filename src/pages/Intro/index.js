import React from "react";
import { SetBackground } from "../../services/SetBackground";

import Title from "../../components/Title";
import Navigation from "../../components/Navigation";
import Categories from "../../components/Categories";

const Intro = () => {
  const title = "10 PYTAŃ / 5 KATEGORII";
  const backwardPath = "/";
  const forwardPath = "/start";
  const exactPath = "/intro";

  SetBackground(1);

  return (
    <React.Fragment>
      <div className="intro-wrapper">
        <Navigation
          backwardPath={backwardPath}
          forwardPath={forwardPath}
          exactPath={exactPath}
        />
        <Title title={title} question={false} />

        <p>WYBIERZ KATEGORIĘ</p>
        <Categories forwardPath={forwardPath} />
      </div>
    </React.Fragment>
  );
};

export default Intro;
