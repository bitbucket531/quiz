import { useContext } from "react";
import { SiteTheme } from "../../context";

export const SetBackground = (page) => {
  const {
    theme,
    previousTheme,
    setPreviousTheme,
    actualSite,
    setActualSite,
  } = useContext(SiteTheme);
  const add = theme + "-site" + page;
  const remove = previousTheme + "-site" + actualSite;
  document.body.classList.add(add);
  if (remove !== add) document.body.classList.remove(remove);
  setActualSite(page);
  setPreviousTheme(theme);
};
