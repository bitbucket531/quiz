import "./styles/global.scss";

import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Drag_and_drop from "./pages/Drag_and_drop";
import Intro from "./pages/Intro";
import Score from "./pages/Score";
import Select from "./pages/Select";
import Start from "./pages/Start";

function App() {
  return (
      <React.Fragment>
        <Router>
          <Switch>
            <Route exact path="/" component={Intro} />
            <Route path="/start" component={Start} />
            <Route path="/select" component={Select} />
            <Route path="/drag" component={Drag_and_drop} />
            <Route path="/score" component={Score} />
          </Switch>
        </Router>
      </React.Fragment>
  );
}

export default App;
